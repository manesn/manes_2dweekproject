﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class EnemyReaction : MonoBehaviour
{

	void Start()
	{

	}

	void Update()
	{

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		// Checking if the overlapped collider is an enemy or not
		if (other.CompareTag("Enemy"))
		{
			// This scene has to be in the Build settings.
			SceneManager.LoadScene("scene1");
		}

		if (other.CompareTag("Coin"))
        {
			other.gameObject.SetActive(false);
		}
	}
}